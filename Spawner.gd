extends Node2D

var delta_sum

# Called when the node enters the scene tree for the first time.
func _ready():
	delta_sum = 4.0


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if delta_sum>4.0:
		delta_sum=0
		var new_x=randi_range(-600,600)
		var new_waste = preload("res://Elements/Waste/waste_base.tscn").instantiate()
		new_waste.position.x=new_x
		new_waste.position.y=position.y
		new_waste.self_modulate.a=255
		add_child(new_waste)
	delta_sum += delta
