@tool

class_name WasteBase

extends Area2D

@export var base_fallspeed = 300.0
@export var max_fallspeed = 500.0
var fallspeed

enum State {FALL, LAY, GROUNDED, PICKED, SMASHED, THROW, THROW_UPDATE, THROWN_BIN, DISAPPEARING, DISAPPEAR}
var state = State.FALL

var lay_wait_timer = null
var destruct_timer = null

var bin_target_node = null
var player = null
var throw_path = null
var make_path_height = null
var make_path_target_x = null
var value_score=50

# Called when the node enters the scene tree for the first time.
func _ready():
	texture_random()
	if get_child_count() == 0:
		var cs2d = CollisionShape2D.new()
		cs2d.shape = CircleShape2D.new()
		cs2d.shape.radius = 10.0
		add_child(cs2d)
	if not Engine.is_editor_hint():
		fallspeed = base_fallspeed
		lay_wait_timer = Timer.new()
		lay_wait_timer.name = "lay"
		add_child(lay_wait_timer)
		destruct_timer = Timer.new()
		destruct_timer.name = "destruct"
		add_child(destruct_timer)
		connect("body_entered", Callable(self, "_check_body_entered"))
		#connect("body_exited", Callable(self, "_check_body_exited"))
		#connect("area_entered", Callable(self, "_check_area_entered"))
		#connect("area_exited", Callable(self, "_check_area_exited"))
		
		#tmp
		bin_target_node = get_tree().get_root().find_child("Poubelle", true, false)
		player=get_tree().get_root().find_child("Player", true, false)
		
		throw_path = player.find_child("throw_path_waste", true, false) 
		if throw_path == null:
			throw_path = preload("res://Elements/Test path waste/path_2d_waste.tscn").instantiate()
	#		get_tree().get_root().add_child(throw_path)
			player.add_child(throw_path)
			throw_path.name = "throw_path_waste"
		
		lay_wait_timer.connect("timeout", Callable(self, "_lay"))

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not Engine.is_editor_hint():
		if state == State.FALL:
			position.y += fallspeed * delta
			fallspeed = lerp(fallspeed, max_fallspeed, 0.8)
			fall_check()
		elif state == State.LAY:
			lay_wait_timer.start(10.0)
			_on_check_timer()
			
			if value_score==30:
				value_score=15
			state = State.GROUNDED
		elif state == State.GROUNDED:
			pass
		elif state == State.SMASHED: #pause or slow before smash
			if bin_target_node != null:
				var target_pos = bin_target_node.get_node("Marker2D").global_position
				var twn = get_tree().create_tween()
				twn.set_ease(Tween.EASE_OUT)
				twn.set_trans(Tween.TRANS_CUBIC)
				twn.tween_property(self, "global_position", target_pos, 0.4)
			else:
				position.y -= fallspeed * delta
				fallspeed = lerp(fallspeed, max_fallspeed, 0.8)
		elif state == State.PICKED:
			if get_tree().get_root().find_child("Player",true,false).find_child("ThrowCurve",true,false)!=null:
				get_tree().get_root().find_child("Player",true,false).find_child("ThrowCurve",true,false).visible=true
			visible = false
			if value_score==50:
				value_score=30
		elif state == State.THROW:
			visible = true
			get_tree().get_root().find_child("Player",true,false).find_child("ThrowCurve",true,false).visible=false
			if bin_target_node != null:
#				var player_pos = player.global_position
#				var target_pos_x = to_local(bin_target_node.get_node("Marker2D").global_position).x
#				throw_path.make_path(Vector2(0,700) - player_pos,target_pos_x)
				throw_path.make_path(make_path_height, make_path_target_x)

				var twn = get_tree().create_tween()
				twn.set_ease(Tween.EASE_OUT)
				twn.set_trans(Tween.TRANS_CUBIC)
				twn.tween_property(throw_path.get_node("PathFollow2D"),"progress_ratio", 1.0, 1.0)
			state = State.THROW_UPDATE
		elif state == State.THROW_UPDATE:
			global_position = throw_path.get_node("PathFollow2D").global_position
		elif state == State.DISAPPEARING:
			#destruct_timer.start()
			var twn = get_tree().create_tween()
			twn.tween_property(self, "modulate", Color(1.0, 0.0, 0.0, 0.15), 2.3)
			var twn2 = get_tree().create_tween()
			twn2.tween_property($Sprite2D, "scale", Vector2(1.3, 1.3), 0.5)
			twn2.tween_property($Sprite2D, "scale", Vector2(0.9, 0.9), 0.5)
			twn2.tween_property($Sprite2D, "scale", Vector2(1.3, 1.3), 0.4)
			twn2.tween_property($Sprite2D, "scale", Vector2(0.9, 0.9), 0.3)
			twn2.tween_property($Sprite2D, "scale", Vector2(1.4, 1.4), 0.2)
			twn2.tween_property($Sprite2D, "scale", Vector2(0.8, 0.8), 0.2)
			twn2.tween_property($Sprite2D, "scale", Vector2(1.5, 1.5), 0.1)
			twn2.tween_property($Sprite2D, "scale", Vector2(0.7, 0.7), 0.1)
			#twn2.play()
			await twn.finished
			state = State.DISAPPEAR
		elif state == State.DISAPPEAR:
			#tmp, use a timer to give last chance to pick item when it is disappearing
			get_tree().get_root().find_child("Player",true,false).life_count-=1
			queue_free()
		elif state == State.THROWN_BIN:
			get_tree().get_root().find_child("audiodunk",true,false).play()
			queue_free()
			

func fall_check():
	pass

func _on_check_timer():
	print(lay_wait_timer.time_left)

func _lay():
	if is_laying():
		state=State.DISAPPEARING

func is_laying():
	return state == State.LAY or state == State.GROUNDED


func is_falling():
	return state == State.FALL


func set_throw_curve(height, target_x):
	make_path_height = height
	make_path_target_x = target_x


func _check_body_entered(body):
	if body.name == "Player":
		return
	else:
		state = State.LAY
		
func _check_area_entered(area):
	if area.get_parent().name == "Player":
		if area.get_parent().timer_collect == true:
			if not area.get_parent().is_on_floor():
				state = State.SMASHED
				fallspeed = base_fallspeed
			else:
				state = State.PICKED
				
func texture_random():
	var texture = randi_range(0,1)
	match texture:
		0:
			$Sprite2D.texture=preload("res://Assets/dechet_toxic-removebg-preview-removebg-preview.png")
#			$Sprite2D.offset.y=-6
#			$Sprite2D.offset.xdd=-10
		1:
			$Sprite2D.texture=preload("res://Assets/dechet recoupé.png")
#			$Sprite2D.offset.y=-1
#			$Sprite2D.offset.x=10


func _on_area_shape_entered(area_rid, area, area_shape_index, local_shape_index):
	if area.name=="Area2D destruc":
		var score=get_tree().get_root().find_child("Score",true,false)
		score.text=str(int(score.text)+value_score)
		state=State.THROWN_BIN
		
		
		
		
