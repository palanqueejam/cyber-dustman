extends Path2D

@export var n : int = 4

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func make_path(begin_pos,target_x):
	if begin_pos is Vector2:
		begin_pos = begin_pos.y
	curve.clear_points()
	position=Vector2.ZERO
#	texture = preload("res://Assets/dotsline2D.png")
#	texture_repeat = CanvasItem.TEXTURE_REPEAT_ENABLED
#	texture_mode = Line2D.LINE_TEXTURE_TILE
	if abs(target_x) < 1:
		curve.add_point(Vector2.ZERO)
	for i in abs(target_x):
		curve.add_point(Vector2(sign(target_x)*i, pow(i,n)*begin_pos/pow(target_x,n)))
	curve.add_point(Vector2(target_x, begin_pos))
	curve.bake_interval=5
	print("hey")
