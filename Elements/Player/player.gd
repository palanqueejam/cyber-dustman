extends CharacterBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const MOVEMENT_SPD = 300.0#150
const FALLING_MAXSPD = 1500.0#680.0 #640#600#380 #310
const FALLING_BASESPD = 400.0#170.0 #100#20#220
const JUMP_POWER = 2500.0 #2100.0#3000.0#1000.0#260.0#60.0
const LIFE_COUNT_MAX = 6

var falling = 0.0
var jumping = false
var life_count
var delta_last_time_on_floor = 10.0
var timer_collect=false
var waste_contact_array = []
var waste_collected_array = []
var waste_collect_limit = 1
var throwing = false
var throw_direction
var target_throw_speed = 350.0
@export var running_song : AudioStreamPlayer

# Called when the node enters the scene tree for the first time.
func _ready():
	life_count = LIFE_COUNT_MAX
	reset()
	$"Area2D (garbage)".connect("area_entered", Callable(self, "_check_area_entered"))
	$"Area2D (garbage)".connect("area_exited", Callable(self, "_check_area_exited"))


func _process(delta):
	process_action_input(delta)
	
	
	
	#get_tree().reload_current_scene()
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	process_movement_input(delta)


func process_movement_input(delta):
	var move = Vector2.ZERO
#	if Input.is_action_pressed("ui_left"):
#		move.x -= 1
#	if Input.is_action_pressed("ui_right"):
#		move.x += 1
#	if Input.is_action_pressed("ui_up"):
#		move.y -= 1
#	if Input.is_action_pressed("ui_down"):
#		move.y += 1

	if Input.is_action_pressed("ui_left"):
		move.x -= MOVEMENT_SPD
	if Input.is_action_pressed("ui_right"):
		move.x += MOVEMENT_SPD
	if Input.is_action_pressed("jump") and (is_on_floor() or (delta_last_time_on_floor < 0.16 and !jumping)):
		trigger_jump()
		
	if is_jumping():
		falling = lerp(falling, 0.0, 10*delta)
		if falling > -18:
			$Sprite2D.scale.x = 1.0
			$Sprite2D.scale.y = 1.0
			jumping = false
			falling = 0
	elif is_on_floor():
		falling = 0
		delta_last_time_on_floor = -delta
		play_walk_song()
	elif falling == 0: #not onfloor and falling == 0
		falling = FALLING_BASESPD
	else:
#		falling = lerp(falling, FALLING_MAXSPD, g.4 * delta)
		falling = lerp(falling, FALLING_MAXSPD, 2.4 * delta)
		#falling = move_toward(falling, FALLING_MAXSPD, 40 * delta)
	#move.y += 220
	move.y = falling
	
#	self.state = State.MOVE #uncomment when move state is implemented (move state = animation + can process attack)
	#move *= MOVEMENT_SPD * delta
	
	var direction = sign(move.x)
	if direction != 0:
		scale.x = direction * abs(scale.x)
		print(scale.x)
		scale.y = direction * abs(scale.y)
		rotation_degrees = 180 / 2 * (1 - direction)
		for c in get_children():
			if c is Camera2D:
				c.scale.x = direction * abs(c.scale.x)
		pass
		if not throwing:
			throw_direction = direction
	
	if Input.is_action_just_released("jump"):
		falling=0.0 
	
	#position += move
	#move_and_collide(move) #move_and_collide doesn't take into account the global_scale of the current object
	up_direction = Vector2.UP
	velocity = move
	move_and_slide()
	
	if move.x == 0.0 or move.y > 0.0:
		stop_walk_song()
	
	if delta_last_time_on_floor < 10:
		delta_last_time_on_floor += delta
	
	
func trigger_jump():
	jumping = true
	falling = -JUMP_POWER
	$Sprite2D.scale.x = 0.92
	$Sprite2D.scale.y = 1.05
	$Jump_sound.play()


func is_jumping():
	return jumping

func reset():
	jumping = false


func process_action_input(delta):
	if life_count<1:
		get_tree().change_scene_to_file("res://Levels/game_over.tscn")
	if Input.is_action_just_pressed("collect") and timer_collect==false:
		timer_collect=true
		#modulate = Color.BLUE
		timer()
	
	var erase_array = []
	for i in waste_collected_array:
		if not is_instance_valid(i):
			erase_array.append(i)
	for i in erase_array:
		waste_collected_array.erase(i)
	for i in waste_contact_array:
		if not is_instance_valid(i):
			erase_array.append(i)
	for i in erase_array:
		waste_contact_array.erase(i)
	
	if throwing: #and not waste_collected_array.is_empty:
		var curve = get_node("ThrowCurve")
		throw_direction = -1 if (to_global(Vector2(curve.target_x + throw_direction * target_throw_speed * delta,0)).x > 1200.0) else 1 if (to_global(Vector2(curve.target_x + throw_direction * target_throw_speed * delta,0)).x < 0.0) else throw_direction
		throw_direction = -1 if (Vector2(curve.target_x + throw_direction * target_throw_speed * delta,0).x > 1200.0/2.8) else 1 if ((Vector2(curve.target_x + throw_direction * target_throw_speed * delta,0)).x < - 1200.0/2.8) else throw_direction
		curve.height = 700 - global_position.y
		curve.target_x = curve.target_x + throw_direction * target_throw_speed * delta
		if Input.is_action_just_pressed("collect"):
			throwing = false
			waste_collected_array[0].state = WasteBase.State.THROW
			#waste_collected_array[0].throw_at(vector_x)
			#var twn = get_tree().create_tween() #tmp
			#twn.tween_property(waste_collected_array[0], "global_position", to_global(Vector2(curve.target_x, curve.height)), 0.9) #tmp
			waste_collected_array[0].set_throw_curve(curve.height, curve.target_x)
			#MAYBE USE: 
			#waste_contact_array.append(waste_collected_array[0]) #notsure
			
			waste_collected_array.remove_at(0)
	if timer_collect and not waste_collected_array.is_empty():
		pass
		#TODO: code waste throw
		var curve = get_node_or_null("ThrowCurve")
		if curve == null:
			curve = preload("res://test_throw_curve.gd").new()
			add_child(curve)
			curve.name = "ThrowCurve"
		curve.n = 4
		curve.height = 700 - global_position.x
		curve.target_x = 0
		throwing = true
		timer_collect = false
	if timer_collect and not waste_contact_array.is_empty() and waste_collected_array.size() < waste_collect_limit:
		var to_erase = []
		for w in waste_contact_array:
			if is_instance_valid(w):
				if w.is_falling() and not is_on_floor():
					w.state = WasteBase.State.SMASHED
					var sf = get_node_or_null("SmashFreeze")
					if sf == null:
						sf = Timer.new()
						sf.wait_time = 0.08 #0.1
						add_child(sf)
						sf.name = "SmashFreeze"
						sf.process_mode = Node.PROCESS_MODE_ALWAYS
					sf.start()
					get_tree().paused = true
					$Sprite2D.process_mode = Node.PROCESS_MODE_ALWAYS
					$Sprite2D.material.set_shader_parameter("invert_color_b_to_w", true)
					$Sprite2D.scale.y = 0.8
					$Sprite2D.scale.x = 1.1
					await sf.timeout
					get_tree().paused = false
					$Sprite2D.process_mode = Node.PROCESS_MODE_INHERIT
					$Sprite2D.scale.y = 1.0
					$Sprite2D.scale.x = 1.0
					$Sprite2D.material.set_shader_parameter("invert_color_b_to_w", false)
				elif w.is_falling() or w.is_laying():
					w.state = WasteBase.State.PICKED
					if waste_collected_array.size() < waste_collect_limit:
						waste_collected_array.append(w)
					to_erase.append(w)
		for w in to_erase:
			waste_contact_array.erase(w)


func timer():
	await get_tree().create_timer(0.5).timeout
	timer_collect=false
	modulate = Color.WHITE


func _check_area_entered(area):
	if area is WasteBase:
		waste_contact_array.append(area)


func _check_area_exited(area):
	if area is WasteBase:
		waste_contact_array.erase(area)
		
func play_walk_song():
	if (Input.is_action_pressed("ui_right") or Input.is_action_pressed("ui_left")) and $Running_song.playing == false:
		$Running_song.play()

func stop_walk_song():
	$Running_song.stop()

#func trigger_attack():
#	$AnimationPlayer.play("Attack")
#	$AnimationPlayer.queue("RESET")
	#change animation depending on level
	
	
#func take_hit():
#	if $AnimationPlayer.current_animation == "Took_Hit" and $AnimationPlayer.is_playing():
#		return
#
#	$AnimationPlayer.play("Took_Hit")
#	life_count -= 1
#	if life_count < 1:
#		get_tree().reload_current_scene()
#
#
##When an enemy is angered and you get knockbacked after hitting them
#func take_knockback():
#	pass
	#tocode

#func _on_HitBox_area_entered(area):
#	if area.is_in_group("hurtbox_enemy"):
#		take_hit()
