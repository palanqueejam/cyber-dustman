@tool
extends Line2D
class_name TestThrowCurve

@export var n : int = 4
@export var target_x : float = 150.0 
@export var height : float = 300.0 #ground.y - player.y

var local_n = 0
var local_target_x = 0
var local_height = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	local_n = 0
	local_target_x = 0
	local_height = 0
	texture = preload("res://Assets/dotsline2D.png")
	texture_repeat = CanvasItem.TEXTURE_REPEAT_ENABLED
	texture_mode = Line2D.LINE_TEXTURE_TILE


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if local_height != height or local_n != n or local_target_x != target_x:
		clear_points()
		local_n = n
		local_target_x = target_x
		local_height = height

#		for i in abs(target_x) + 1:
#		#for i in height/2:
#			add_point(Vector2(sign(target_x)*i, pow(i,n)*height/pow(target_x,n)))
#		if target_x < 1 or target_x > -1:
#			add_point(Vector2(target_x, height))
#		#bake?
		
		if abs(target_x) < 1:
			add_point(Vector2.ZERO)
		for i in abs(target_x):
			add_point(Vector2(sign(target_x)*i, pow(i,n)*height/pow(target_x,n)))
		add_point(Vector2(target_x, height))
