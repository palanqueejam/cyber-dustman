extends TextureRect
class_name Heart
@export var heartfull :Texture = preload("res://Assets/coeur_plain-removebg-preview-removebg-preview.png")
@export var heartmid : Texture = preload("res://Assets/demi_coeur-removebg-preview-removebg-preview.png")
@export var heartempty : Texture = preload("res://Assets/coeur_vide-removebg-preview-removebg-preview.png")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func display_full():
	texture=heartfull
	
func display_mid():
	texture=heartmid
	
func display_empty():
	texture=heartempty
