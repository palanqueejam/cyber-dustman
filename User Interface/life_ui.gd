extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var current_heart
var total_heart


#export(PackedScene) var player
@export var player_np : NodePath
var player = null

# Called when the node enters the scene tree for the first time.
func _ready():
	$HBoxContainer.scale *= 2
	player = get_node(player_np)
	if player != null:
		#heart_shown = player.life_count
		await get_tree().process_frame
		update_nb_heart_displayed()
	print(player)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if player != null:
		#print(player.life_count)
		if current_heart != player.life_count:
			update_nb_heart_displayed()

#Max heart displayable = 10
#Fixable by creating the heart in the code instead of in the scene
func update_nb_heart_displayed():
	
	current_heart = player.life_count
	total_heart = player.LIFE_COUNT_MAX
	var boucle=total_heart/2
	if total_heart%2==1:
		boucle+=1
	#hide_all_heart()
	if $HBoxContainer.get_child_count() < total_heart:
		
		for i in boucle:
			$HBoxContainer.add_child(preload("res://User Interface/heart.gd").new())
				
#		total_heart/2 if total_heart%2==0 else total_heart/2+1
						
	for i in current_heart/2:
		$HBoxContainer.get_child(i).display_full()
	var ajout=0
	if current_heart % 2 == 1:
		$HBoxContainer.get_child(current_heart/2).display_mid()
		ajout+=1
	for i in (total_heart-current_heart) / 2:
		$HBoxContainer.get_child(i+ajout+current_heart/2).display_empty()
		
func hide_all_heart():
	for c in $HBoxContainer.get_children():
		c.visible = false
